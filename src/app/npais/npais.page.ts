import { Component, OnInit } from '@angular/core';
import {SrvcovidService} from '../srvcovid.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-npais',
  templateUrl: './npais.page.html',
  styleUrls: ['./npais.page.scss'],
})
export class NpaisPage implements OnInit {
  paises : any;
  constructor(
    public service : SrvcovidService,
    public router : Router
    
  ) { }

  ngOnInit() {this.service.getPaises()
    .subscribe(
      (data)=> {
        this.paises= data;
        this.paises.sort((a,b) => a.Country.toLowerCase() > b.Country.toLowerCase() ?1 :-1);
        let fselected = [];
        fselected= this.service.getSelectedCountries();
        for (let i = 0;  i < fselected.length; i++){
          for(let j = 0; j < this.paises.length; j++){
          if(fselected[i]['ISO2'] == this.paises[j]['ISO2']){
            this.paises[j]['isChecked'] = true;
            break;
          }
        }
      }
      },
      (error)=>{
        console.error(error);
      }
    )
  
  }

  onSubmit() {
    let data =[];
    this.paises.forEach(element => {
      if(element.isChecked)
         data.push(element); 
    });
    this.service.setSelectedCountries(data);
    this.router.navigate(['']);
  }

}
