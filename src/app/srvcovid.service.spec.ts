import { TestBed } from '@angular/core/testing';

import { SrvcovidService } from './srvcovid.service';

describe('SrvcovidService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SrvcovidService = TestBed.get(SrvcovidService);
    expect(service).toBeTruthy();
  });
});
