import { Component } from '@angular/core';
import {SrvcovidService} from '../srvcovid.service';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  summary : any[]=[];
  lastDate : null;

  constructor(
    public service : SrvcovidService
  ) {}
  ngOnInit(){
    this.service.getSummary()
    .subscribe(
      (data)=> {
        this.summary= data['Global'];
        this.lastDate= data['Date']
      },
      (error)=>{
        console.error(error);
      }
    )
  }

}
